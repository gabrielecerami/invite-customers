"""
This file contains the config class that will contain the
configuration
"""

import logging
import os
import yaml

from urllib.parse import urlparse, urlunparse


class Config():
    """
    The config class, an instance will be passed to all other class
    as configuration. A static attribute contains the defaults
    """

    log = logging.getLogger(__name__)

    defaults = type("Defaults", (), {
        'center': "53.339428,-6.257664",
        'max_distance': "100",
        'customers_source_url': "customers.txt",
        'output_file': "invitees-list.txt",
        'mean_earth_radius': 6371
    })

    def __init__(self, cli_args):
        """
        Standard init method, will call all others in chain.
        Establish a precedence between defaults, file config and command
        line config, drives the other methods
        """

        config = dict(self.defaults.__dict__)

        if cli_args.config_file:
            file_config = self.load_config_file(cli_args.config)
            self.log.info("Loading config from file: %s", cli_args.config)
            config.update(file_config)

        config.update(cli_args.__dict__)

        self.load_config(config)

    def load_config(self, config):
        """
        From the config dict, mangles and creates the others methods in the
        config class, for example converts comma separated strings to tuples
        :parameter config: A dict with the configuration
        :return: None
        """
        self.center = tuple(map(float, config['center'].split(',')))

        self.customers_source_url = config['customers_source_url']
        parsed_url = urlparse(self.customers_source_url, scheme='file')
        if parsed_url.scheme == 'file' and not os.path.isabs(parsed_url.path):
            path = os.path.join(os.getcwd(), parsed_url.path)
            self.log.info("Customer file expanded to absolute path %s", path)
            parsed_url = parsed_url._replace(path=path)
        self.customers_source_url = urlunparse(parsed_url)

        self.output_file = config['output_file']

        self.max_distance = float(config['max_distance'])

        self.mean_earth_radius = config['mean_earth_radius']

    def load_config_file(self, path):
        """
        Load a yaml file containing config attribute
        :parameter path: A string with the path to the config file
        :return: A dict with configuration
        """
        try:
            with open(path) as config_file:
                config = yaml.safe_load(config_file.read())
        except yaml.YAMLError:
            raise Exception("Invalid config file")

        return config
