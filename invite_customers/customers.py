"""
This file contains the customer class with all customer related information
"""

import json
import logging
import math

from urllib.request import urlopen


class Customers(list):
    """
    Customer class, inherits from list, will contain a list of dictionaries,
    each with a single customer information
    """

    log = logging.getLogger(__name__)

    def __init__(self, config):
        """
        pretty standard init, drives the rest of the methods, so the
        instantiation is enough
        """
        super(Customers, self).__init__()
        self.config = config
        customers = self.get_customers_file()
        self.build_customers_db(customers)

    def get_customers_file(self):
        """
        get the contents of the customer.txt file from the url specified
        in configuration. Works with local and remote locations
        :return: A list of string, one customer information per line,
        typically json
        """
        self.log.info("Getting customers from %s",
                      self.config.customers_source_url)
        with urlopen(self.config.customers_source_url) as customer_file:
            customers = customer_file.read().decode('UTF-8')

        return customers.split('\n')

    @staticmethod
    def transform_record(record):
        """
        A static method to transform customer information. String to float for
        the coordinates, then coordinate from degree to radians
        :parameter record: A dict with customer information
        """
        record['latitude'] = float(record['latitude'])
        record['longitude'] = float(record['longitude'])
        record['radians_latitude'] = math.radians(record['latitude'])
        record['radians_longitude'] = math.radians(record['longitude'])
        return record

    def build_customers_db(self, customers):
        """
        Builds the list of customers from customer content. The list is then
        sorted by user id.
        :parameter customers: A list of strings, in JSON format, one customer
        per line
        :return: None
        """

        for line in customers:
            try:
                record = json.loads(line)
            except json.decoder.JSONDecodeError:
                self.log.error("Invalid customer record %s: not loaded", line)
                continue

            record = self.transform_record(record)
            self.append(record)

        self.sort(key=lambda x: x['user_id'])
        self.log.info("Added %d customers", len(self))
