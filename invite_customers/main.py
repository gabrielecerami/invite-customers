"""
This file will be called from the entry point script
"""

import argparse
import logging

from invite_customers.config import Config
from invite_customers.customers import Customers
from invite_customers.invitation import Invitation


def arg_parser(cmd_line=None):
    """
    Parses command line arguments.
    :parameter cmd_line: A string. If specified, the string will be parsed
    if not, the system command line will be parsed.
    :return: A Namespace object with the parsed configuration
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        dest='config_file',
                        help="A yaml file to load config from")
    parser.add_argument('--max-distance',
                        default=Config.defaults.max_distance,
                        help=("Maximum distance to allow invite"
                              " (in kilometers)"))
    parser.add_argument('--center',
                        default=Config.defaults.center,
                        help=("The latitude longitude "
                              "coordinates for the center "
                              "(comma separated)"))
    parser.add_argument('--customers-from',
                        dest='customers_source_url',
                        default=Config.defaults.customers_source_url,
                        help=("A URL containing the text "
                              "file with customer info"))
    parser.add_argument('--invitees-output',
                        dest='output_file',
                        default=Config.defaults.output_file,
                        help=("Path to the output text file "
                              "with the list of invitees"))
    if cmd_line is not None:
        args = parser.parse_args(cmd_line.split())
    else:
        args = parser.parse_args()

    return args


def main(cmd_line=None):
    """
    The main entry point
    :parameter cmd_line: A string to pass as command line to parse
    :return: None
    """
    log = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)
    args = arg_parser(cmd_line=cmd_line)
    log.info("Getting Configuration")
    config = Config(args)
    log.info("Gerring customers")
    customers = Customers(config)
    log.info("Analyzing invitation")
    invitation = Invitation(config)
    log.info("Dumping invitees list")
    invitation.dump_invitees(customers)


if __name__ == '__main__':
    main()
