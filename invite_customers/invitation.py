"""
This file contains the Invitation class with information of an event
and the criteria for invitation of customers
"""

import logging
import math


class Invitation():
    """
    The class contain mainly the criteria for invitation of a customer
    and the method to filter lists of customers for invitation
    """

    log = logging.getLogger(__name__)

    def __init__(self, config):
        """
        Standard init method, just fills basic attribute for filtering
        """
        for attribute in ['max_distance', 'center']:
            try:
                setattr(self, attribute, getattr(config, attribute))
            except AttributeError:
                pass

        if hasattr(self, 'center'):
            center_phi, center_lmbd = map(float, self.center)
            self.center_lmbd = math.radians(center_lmbd)
            self.center_phi = math.radians(center_phi)
            self.log.info("loaded center coordinates: %s", self.center)

        self.output_file = config.output_file
        self.mean_earth_radius = config.mean_earth_radius

    def within_distance(self, customer):
        """
        A boolean method to verify if the distance from the center
        is less than the max distance allowed.
        Uses great-circle formula for high-precision machines.
        :parameter customer: A dict with customer information, should contain
        latitute and longitude information in radians.
        :return: A bool, True if the customer are within distance from the
        center, false otherwise
        """
        lmbd = customer['radians_longitude']
        phi = customer['radians_latitude']

        central_angle = \
            math.acos((math.sin(self.center_phi) * math.sin(phi))
                      + (math.cos(self.center_phi) * math.cos(phi)
                      * math.cos(self.center_lmbd - lmbd)))

        distance = self.mean_earth_radius * central_angle

        if distance <= self.max_distance:
            self.log.info("Inviting customer '%s'", customer['name'])
            return True

        self.log.info("customer '%s' too distant", customer['name'])
        return False

    def get_invitees(self, customers):
        """
        Get the list of invitees based on criteria. Right now only distance
        is implemented
        :parameter customers: A list of dictionaries, on dict per customer
        :return: A list of tuples with the filtered customers: user id and name
        """
        invitees = []
        for customer in customers:
            if hasattr(self, 'max_distance') and hasattr(self, "center") \
                    and self.within_distance(customer):
                invitees.append((customer['user_id'], customer['name']))

        return invitees

    def dump_invitees(self, customers):
        """
        Dump invitees information on a text file, one customer per line, user
        id and name. The file path is taken from configuration
        :parameter customers: A list of dictionaries, one dict per customer.
        :return: None
        """
        invitees = self.get_invitees(customers)
        output = "\n".join(map(lambda x: "{} {}".format(x[0], x[1]), invitees))

        self.log.info("Dumping invitees list to %s", self.output_file)
        with open(self.output_file, 'w') as output_file:
            output_file.write(output)
