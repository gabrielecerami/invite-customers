import os
import tempfile
import unittest

from invite_customers.customers import Customers
from unittest.mock import patch

test_cases = {
    'correct': '''
{ "user_id": 2, "name": "a", "longitude": 1.1, "latitude": -2.2 }
{ "user_id": 1, "name": "b", "longitude": 3.3, "latitude": -4.4 }
'''
}


class TestCustomers(unittest.TestCase):

    def setUp(self):

        self.paths = {
            'correct': tempfile.mkstemp(prefix="customers-", suffix=".txt")[1]
        }
        for case_name, file_name in self.paths.items():
            with open(file_name, 'w') as case_file:
                case_file.write(test_cases[case_name])

            config = type("Config", (), {
                'customers_source_url': "file://{}".format(file_name)
            })
            setattr(self, "config_file_{}".format(case_name), config)
            config = type("Config", (), {
                'customers_source_url': "file://{}".format(file_name)
            })
            setattr(self, "config_http_{}".format(case_name), config)

    def tearDown(self):
        for file_name in self.paths.values():
            os.unlink(file_name)

    def test_customers_from_local(self):
        customers = Customers(self.config_file_correct)
        self.assertIsInstance(customers, Customers)
        self.assertEqual(len(customers), 2)
        for customer in customers:
            self.assertIsInstance(customer, dict)
            self.assertIn('user_id', customers[0])

    @patch('urllib.request.urlopen')
    def test_customers_from_remote(self, mock_urlopen):
        mock_urlopen.return_value = ""
        customers = Customers(self.config_http_correct)
        self.assertIsInstance(customers, Customers)
        self.assertEqual(len(customers), 2)
        for customer in customers:
            self.assertIsInstance(customer, dict)
            self.assertIn('user_id', customers[0])
