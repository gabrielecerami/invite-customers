import unittest

from argparse import Namespace
from invite_customers.main import (arg_parser, main as code_main, Config,
                                   Customers, Invitation)
from unittest.mock import patch


class TestMain(unittest.TestCase):

    @patch.object(Config, '__init__', autospec=True, return_value=None)
    @patch.object(Customers, '__init__', autospec=True, return_value=None)
    @patch.object(Invitation, '__init__', autospec=True, return_value=None)
    @patch.object(Invitation, 'dump_invitees', autospec=True,
                  return_value=None)
    def test_simple_call(self, mock_invitation_dump, mock_invitation,
                         mock_customers, mock_config):
        line = ""
        code_main(cmd_line=line)
        self.assertTrue(mock_config.called)
        self.assertTrue(mock_customers.called)
        self.assertTrue(mock_invitation.called)
        self.assertTrue(mock_invitation_dump.called)


class TestArgParser(unittest.TestCase):

    def test_defaults(self):
        line = ""
        cli_args = arg_parser(cmd_line=line)
        self.assertIsInstance(cli_args, Namespace)
        assert hasattr(cli_args, 'max_distance')
        self.assertEqual(cli_args.max_distance, "100")
        assert hasattr(cli_args, 'center')
        self.assertEqual(cli_args.center, "53.339428,-6.257664")

    def test_error(self):
        line = "--config --other"
        with self.assertRaises(SystemExit):
            arg_parser(cmd_line=line)

    def test_optionals(self):
        self.maxDiff = None
        line = ("--config config.yaml"
                " --max-distance 10"
                " --center 53.339428,-6.257664"
                " --customers-from ftp://intercom/customers"
                " --invitees-output invitees-list.txt")

        expected = Namespace(center="53.339428,-6.257664",
                             max_distance="10",
                             config_file="config.yaml",
                             output_file="invitees-list.txt",
                             customers_source_url="ftp://intercom/customers"
                             )
        cli_args = arg_parser(cmd_line=line)
        self.assertIsInstance(cli_args, Namespace)
        self.assertEqual(cli_args, expected)
