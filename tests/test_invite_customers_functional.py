import os
import pytest
import tempfile

from invite_customers.main import main as code_main
from .test_cases import customers

expout = '''4 Ian Kehoe
5 Nora Dempsey
6 Theresa Enright
8 Eoin Ahearn
11 Richard Finnegan
12 Christina McArdle
13 Olive Ahearn
15 Michael Ahearn
17 Patricia Cahill
23 Eoin Gallagher
24 Rose Enright
26 Stephen McArdle
29 Oliver Ahearn
30 Nick Enright
31 Alan Behan
39 Lisa Ahearn'''


@pytest.fixture
def customer_path():
    __, customer_path = tempfile.mkstemp(prefix="customers-", suffix=".txt")
    with open(customer_path, "w") as customer_file:
        customer_file.write(customers)

    yield customer_path

    os.unlink(customer_path)


def test_invite_customers_correct(customer_path):
    __, out_path = tempfile.mkstemp(prefix="invitees-", suffix=".txt")
    path = customer_path
    cmd_line = ("--customers-from {} "
                "--invitees-output {} "
                "".format(path, out_path))
    code_main(cmd_line=cmd_line)
    with open(out_path) as out_file:
        output = out_file.read()

    assert output == expout, "Wrong output"


@pytest.mark.xfail(reason="Not Implemented", run=False)
def test_invite_customer_missing_info(customer_file):
    assert False
