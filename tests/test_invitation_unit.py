import math
import os
import tempfile
import unittest
import pytest

from invite_customers.invitation import Invitation

test_cases = {
    'correct': [
        {
            "user_id": 1,
            "name": "a",
            "radians_longitude": math.radians(0.1),
            "radians_latitude": math.radians(-0.2)
        },
        {
            "user_id": 2,
            "name": "b",
            "radians_longitude": math.radians(0.3),
            "radians_latitude": math.radians(-0.4)
        },
        {
            "user_id": 3,
            "name": "b",
            "radians_longitude": math.radians(1.0),
            "radians_latitude": math.radians(-1.0)
        }
    ]
}


class TestInvitation(unittest.TestCase):

    def setUp(self):

        self.paths = {
            'correct': tempfile.mkstemp(prefix="invitees-", suffix=".txt")[1]
        }
        for case_name, file_name in self.paths.items():
            config = type("Config", (), {
                'max_distance': 100,
                'center': (0.0, 0.0),
                'output_file': file_name,
                'mean_earth_radius': 6371
            })
            setattr(self, 'config_{}'.format(case_name), config)

    def tearDown(self):
        for file_name in self.paths.values():
            os.unlink(file_name)

    def test_basic_instance(self):
        invitation = Invitation(self.config_correct)
        assert hasattr(invitation, 'center')
        assert hasattr(invitation, 'center_lmbd')
        assert hasattr(invitation, 'center_phi')
        assert hasattr(invitation, 'max_distance')

    def test_get_invitees_someone(self):
        invitation = Invitation(self.config_correct)
        invitation.dump_invitees(test_cases['correct'])

        expout = "1 a\n2 b"
        with open(self.paths['correct']) as output_file:
            self.assertEqual(output_file.read(), expout)

    @pytest.mark.xfail(reason="Not implemented", run=False)
    def test_get_invitees_everyone(self):
        # TODO: assert order by id
        assert False

    @pytest.mark.xfail(reason="Not implemented", run=False)
    def test_get_invitees_empty(self):
        # TODO: assert order by id
        assert False
