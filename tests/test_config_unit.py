import os
import pytest
import unittest

from invite_customers.config import Config


class TestConfig(unittest.TestCase):

    def test_basic_instance_from_defaults(self):
        cli_args = type("Namespace", (), {
            'config_file': ''
        })
        config = Config(cli_args)
        assert hasattr(config, 'center')
        self.assertIsInstance(config.center, tuple)
        self.assertEqual(config.center, (53.339428, -6.257664))
        assert hasattr(config, 'max_distance')
        self.assertIsInstance(config.max_distance, float)
        self.assertEqual(config.max_distance, 100)
        assert hasattr(config, 'customers_source_url')
        self.assertIsInstance(config.customers_source_url, str)
        path = os.path.join(os.getcwd(), "customers.txt")
        self.assertEqual(config.customers_source_url, "file://{}".format(path))
        assert hasattr(config, 'output_file')
        self.assertIsInstance(config.output_file, str)
        self.assertEqual(config.output_file, "invitees-list.txt")

    @pytest.mark.xfail(reason="Not implemented", run=False)
    def test_basic_instance_from_cli_args(self):
        assert False
