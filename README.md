Invite Customers
================

This project is used to invite customers who lives within a certain radius from a center
It accept a text file with one line per customer with information json format, and output
the list of customers in txt format, one invitee per line, with id and name
The customer information must contain at least:
 - latitude
 - longitude
 - name
 - id

Requirements
============

The project requires pyyaml, automatically installed.
For testing, tox and pytest are required

Design and Architecture
=======================

The project code is separated into four files

- config.py will handle project configuration
- main.py will contain the main logic and the argument parsing
- customers.py will deal with the list of customers.
- invitation.py will contain a class for invitation parameters and how to calculate the invitees


Installation
============

Create a python virtual environment

```bash
python3 -m virtualenv -p python3 venv
```

Activate it

```bash
source venv/bin/activate
```

To install the project, just launch

```bash
python setup.py install
```


Launch

```bash
deactivate
```

to deactivate the virtual environment


Usage
=====

the installation will create a command called invite_customers.
The command takes two optional arguments:
- the configuration file path
- the maximum distance allowed from the center
- the center coordinate
- the url for the list of customers
- the output file path


For example

```bash
invite-customers --customers-from https://s3.amazonaws.com/customers.txt
```

will use all the defaults, grab the customer list from a site and write to invitees-list.txt
in current directory

```bash
invite-customers -c other_conf.yaml
```

or

```bash
invite-customers --center La,Lo --distance 100 --customers-from customers_file.txt --output invitee.txt
```

Testing
=======

For testing tox is required, it can be installed in the virtual environment while active with

```bash
pip install tox
```

Tox will then automatically install all the other dependencies.


To test the project, it's enough to launch

```bash
tox
```
without any arguments.
